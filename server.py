import pymysql
import os
import optparse

from twisted.internet import reactor
from twisted.internet.protocol import Factory , Protocol
from common import COMMANDS, display_message, validate_file_md5_hash, get_file_md5_hash, read_bytes_from_file, clean_and_split_input


class IphoneChat(Protocol):
    
    #Pentru viitor inlocuieste localhost cu IP-ul calculatorului pe care exista serverul
    
    nameOfUser = "dasads"
    beautifulnames=[]
    groups = []
    messages = [()]
    locations = []
    mylocation = (1,1,1)
    db = pymysql.connect("localhost","testuser2","test123","testasd")
    #in cazul in care se face o conexiune
    def connectionMade(self):
        self.factory.clients.append(self)
        print "cliens are " ,self.factory.clients
    
    #in cazul in care se pierde o conexiune
    def connectionLost(self , reason):
        self.factory.clients.remove(self)
        if self.nameOfUser == "dasads" :
            self.nameOfUser = "fals"
        else:
            self.beautifulnames.remove(self.nameOfUser)
            #  if self.mylocation == (1,1,1) :
        #    self.nameOfUser = "fals"
                #else:
        #self.locations.remove(self.mylocation)
        print "clients are " ,self.beautifulnames
    #ca sa placa la ochiul tau cum sunt utilizatorii
        #update 1:solved users name not coinciding
    #this is the main function that parses everything that comes to the server
    def dataReceived(self,data):
        a = data.split(':')
        contor = 0
        print a
        if self.db == None :
            self.transport.write("Error with the database")
        else:
            if len(a) > 1 :
                command = a[0]
                content = a[1]
                
                msg=""
                isthere =False
                indexReceiver = 0
                if command == "iam" :
                    #self.factory.clients.remove(self)
                    self.name = content
                    for nume2 in self.beautifulnames :
                        if nume2 == content:
                            isthere = True
                    else:
                        if isthere == False:
                            msg = self.name + " has joined"
                            #update 2 : the issue was right here[instead of nameOfUser] it was self.nameOfUser]
                            self.nameOfUser = self.name
                            self.beautifulnames.append(self.nameOfUser)
                            print "after calling iam,clients are " ,self.beautifulnames
                            self.transport.write(msg + '\n')
                elif command == "login" :
                    self.db = pymysql.connect("localhost","testuser2","test123","testasd")
                    name = a[1]
                    password = a[2]
                    extra = a[3]
                    cursor = self.db.cursor()
                    sql = "SELECT name_person,password FROM user_profile_info WHERE name_person = '%s' AND password = '%s' " %(name,password)
                    self.name = content
                    try:
                        cursor.execute(sql)
                        results = cursor.fetchall()
                        line = 'user:'+results[0][0]
                        msg = self.name + " has joined"
                        #update 2 : the issue was right here[instead of nameOfUser] it was self.nameOfUser]
                        self.nameOfUser = self.name
                        self.beautifulnames.append(self.nameOfUser)
                       
                        self.transport.write('you are succhessfully logged in\n')
                    except :
                        self.transport.write('We can not log you in')
                elif command == "signup":
                    cursor = self.db.cursor()
                    nameToAdd = a[1]
                    emailToAdd = a[2]
                    passwordToAdd = a[3]
                    phoneToAdd = a[4]
                    ceva = a[5]
                    getEverything = "SELECT name_person,person_email,password,phone_number FROM user_profile_info"
                    cursor.execute(getEverything)
                    results = cursor.fetchall()
                    problem = False
                    for resultRow in results :
                        nameFromTable = resultRow[0]
                        emailFromTable = resultRow[1]
                        passwordFromTable = resultRow[2]
                        phoneFromTable = resultRow[3]
                        if nameToAdd == nameFromTable :
                            #self.transport.write('you weren\'t successfully added \n')
                            problem = True
                        if emailToAdd == emailFromTable :
                            #self.transport.write('you weren\'t successfully added \n')
                            problem = True
                        if phoneToAdd == phoneFromTable :
                            #self.transport.write('you weren\'t successfully added when checking the database\n')
                            problem = True
                                
                    if problem == False :
                        sqlQuery = "INSERT INTO user_profile_info (name_person,person_email,password,phone_number) VALUES ('%s','%s','%s','%s' )" %(nameToAdd,emailToAdd,passwordToAdd,phoneToAdd)
                        try :
                            cursor.execute(sqlQuery)
                            self.db.commit()
                            self.transport.write('you were successfully added\n')
                        except pymysql.err.IntegrityError :
                            #self.transport.write('you weren\'t successfully added after the verification\n')
                            self.db.rollback()
                    else :
                        self.transport.write('you weren\'t successfully added\n')
                elif command == "addfriend" :
                    firstPerson = a[1]
                    secondPerson = a[2]
                    element = a[3]
                    found1 = False
                    found2 = False
                    cursor = self.db.cursor()
                    sqlQuery = "SELECT id_person,name_person FROM user_profile_info "
                    try:
                        cursor.execute(sqlQuery)
                        #we have the first person for some reason the where clause fucks up
                        results = cursor.fetchall()
                        count = results.count
                        for resultRow in results :
                            nameFromTable = resultRow[1]
                            if nameFromTable == firstPerson :
                                firstId = resultRow[0]
                                found1 = True
                            if nameFromTable == secondPerson :
                                secondId = resultRow[0]
                                found2 = True
                        if found1 == True and found2 == True :
                            #self.transport.write("We have both of them\n")
                            sql = "INSERT INTO user_friends_info (first_person_id,second_person_id) VALUES ('%s','%s')" %(firstId,secondId)
                            try :
                                cursor.execute(sql)
                                self.db.commit()
                                sqltwo ="INSERT INTO user_friends_info (first_person_id,second_person_id) VALUES ('%s','%s')" %(secondId,firstId)
                                try:
                                    cursor.execute(sqltwo)
                                    self.db.commit()
                                    self.transport.write('You are now friends\n')
                                except:
                                    print('I don')
                            except pymysql.err.IntegrityError :
                                self.transport.write('Relationship not added\n')
                                self.db.rollback()
                        elif found1 == True and found2 == False:
                            self.transport.write("We don't have the second\n")
                        elif found1 == False and found2 == True :
                            self.transport.write("We don't have the first\n")
                            #else:
                            #  self.transport.write("We don't have them\n")
                    except pymysql.IntegrityError :
                        self.transport.write('We don\'t have that user\n')
                        self.db.rollback()
                elif command == "setcoordinates" :
                    name = content
                    x = a[2]
                    y = a[3]
                    found = 0
                    tuple = (name,x,y)
                    if self.locations == [] :
                        #self.transport.write('it is empty bro')
                        self.mylocation = tuple
                        self.locations.append(tuple)
                        self.transport.write('you added your coordinates \n')

                    else:
                        for location in self.locations :
                            if location[0] == name :
                                self.locations.remove(location)
                                self.locations.append(tuple)
                                self.transport.write('you replaced your coordinates \n')
                                found = 1
                        if found == 0 :
                            self.mylocation = tuple
                            self.locations.append(tuple)
                            self.transport.write('you added your coordinates \n')
                elif command == "getcoordinates" :
                    str1 = '\n '.join(':'.join(str(y) for y in x) for x in self.locations)
                    self.transport.write('Coordinates \n'+ str1 )
                elif command == "msg" :
                    self.name = content
                    msg = self.nameOfUser + " : " + content
                    print msg
                    self.transport.write(msg + '\n')
                #asta e metoda de baza a acestei aplicatii si din fericire merge,in sfarsit am facut-o sa mearga.
                elif command == "msgtouser" :
                    #cum sa pierzi o zi intreaga nerealizand ca vectorul tau de clienti era si de stringuri dar si de utilizatori efectivi
                    #ideea e ca tu cand vroiai sa  trimiti faceam ceva de genul apeleaza o functie de trimitere pe un obiect care trebuia
                    #sa fie "client" dar erau si stringuri pe acolo si le facea pe toate stringuri
                    #si de aceeaam facut doi vectori,unul cu obiecte de tipul client si unul cu numele lor FRUMOASE
                    content = a[3]
                    sender = a[1]
                    receiver = a[2]+'\r\n'
                    goodboy = a[2]
                    ceva = a[4]
                    msg2="message:from:"+sender+":to:"+goodboy+":with:"+content
                    msg="You just sent a message"+'\n'+" message:from:"+sender+":to:"+goodboy+":with:"+content
                    #print msg
                    self.transport.write(msg+'\n')
                    for c in self.beautifulnames :
                        if c == receiver :
                            print "we have that user",indexReceiver
                            self.factory.clients[indexReceiver].transport.write('you have a message\n')
                            self.factory.clients[indexReceiver].transport.write(msg2+'\n'    )
                        else :
                            indexReceiver+=1
                #ca sa faci un grup,nu am facut verificarea daca exista sau nu pentru ca foarte probabil o sa ii aleg
                #in aplicatia din lista de utilizatori online
                #form makegroup:'nameofgroup':'whotoaddtothegroup':'anythingyouwanthere'
                elif command =="makegroup":
                    thisGroup = ()
                    nameofGroup = a[1]
                    member = a[2]
                    ceva = a[3]
                    cursor = self.db.cursor()
                    sqlQuery = "SELECT id_person FROM user_profile_info WHERE name_person = '%s'" %(member)
                    try:
                        cursor.execute(sqlQuery)
                        results = cursor.fetchall()
                        
                        for result in results :
                            idFromTable = result[0]
                            sql = "INSERT INTO user_groups_info (group_name , person_id) VALUES ('%s','%s')" %(nameofGroup,idFromTable)
                            try :
                                cursor.execute(sql)
                                self.db.commit()
                                self.transport.write('You are now part of the group\n')
                            except pymysql.err.IntegrityError :
                                self.transport.write('Relationship not added\n')
                                self.db.rollback()
                    except:
                        self.transport.write('We have an error here')
                #ca sa trimiti un mesaj la grup,oarecum acum verific ,chiar daca nu trebuie eu tot verific
                #form msgtogroup:'nameofgroup':'message':'personsendingmessage':'anythingyouwanthere'
                elif command == "msgtogroup":
                    cursor = self.db.cursor()
                    nameofGroup = a[1]
                    msg = a[2]
                    person1 = a[3]
                    ceva = a[4]
                    #indexR = 0
                    #aici intra
                    #self.transport.write('go here')
                    #self.transport.write(person1)
                    sql = "SELECT id_person FROM user_profile_info WHERE name_person = '%s'" %(person1)
                    try:
                        cursor.execute(sql)
                        #self.transport.write('intra aici')
                        #result = cursor.fetch()
                        results = cursor.fetchall()
                        for result in results :
                            idOF = result[0]
                            intIDOF = int(str(result[0]))
                    #self.transport.write('sads')
                    #self.transport.write(str(result[0]) + ' dsadsa')
                            sqltwo = "SELECT user_profile_info.name_person , user_groups_info.group_name FROM user_profile_info , user_groups_info WHERE user_groups_info.group_name = '%s' AND user_profile_info.id_person = user_groups_info.person_id AND user_profile_info.id_person <> '%d'" %(nameofGroup ,intIDOF)
                            try :
                                cursor.execute(sqltwo)
                                resultsNew = cursor.fetchall()
                                for result in resultsNew :
                                    person_name = result[0]
                                    if person_name[0:1] == ' ':
                                        
                                        stringOther = str(person_name[1:]) +'.groupView\r\n'
                                        stringPerson = str(person_name[1:]) + '\r\n'
                                    else:
                                        stringOther = str(person_name) +'.groupView\r\n'
                                        stringPerson = str(person_name) + '\r\n'
                                        stringsecView = str(person_name) +'.secView\r\n'
                                    
                                #self.transport.write(stringPerson+'online personwith nice name\n')
                                #self.transport.write(stringOther+'online person with not so nice name\n')
                                    indexR = 0
                                    for people in self.beautifulnames :
                                        if people == stringPerson and people != person1 + '\r\n':
                                            #self.transport.write('he is online '+ str(people))
                                            self.transport.write('person is'+people + '\n')
                                            self.factory.clients[indexR].transport.write('you have a message from the group:' + str(nameofGroup) +'and person:'+str(person1)+' with the message\n')
                                            self.factory.clients[indexR].transport.write(str(msg) + '\n')
                                            self.transport.write('you sent it successfully')
                                            indexR += 1
                                        elif people == stringOther and people != person1 + '\r\n' :
                                            self.transport.write('person is'+people+ '\n')
                                            self.factory.clients[indexR].transport.write('you have a message from the group:' + str(nameofGroup) +'and person:'+str(person1)+' with the message\n')
                                            self.factory.clients[indexR].transport.write(str(msg) + '\n')
                                            self.transport.write('you sent it successfully')
                                            indexR += 1
                                        elif people == stringsecView and people != person1 + '\r\n' :
                                            self.transport.write('person is'+people+ '\n')
                                            self.factory.clients[indexR].transport.write('you have a message from the group:' + str(nameofGroup) +'and person:'+str(person1)+' with the message\n')
                                            self.factory.clients[indexR].transport.write(str(msg) + '\n')
                                            self.transport.write('you sent it successfully')
                                            indexR += 1
                                        else :
                                            indexR += 1
                        
                            #self.transport.write(str(person_name) + ' ')
                            except :
                                sel.transport.write('eroare la al doilea try')
                    except:
                        self.transport.write('eroare ;la primul ty')
                #comanda asta ar trebui sa imi returneze utilizatorii intr-un format human readable
                #comanda asta este pentru a vedea utilizatorii care sunt online in aplicatie
                #form mygroups:'nameofPerson':'anythingyouwanthere'
                #same thing with \r\n
                elif command == "mygroups":
                    self.db = pymysql.connect("localhost","testuser2","test123","testasd")
                    cursor = self.db.cursor()
                    myName = a[1]
                    ceva = a[2]
                    names = []
                    usercoordinates  = [()]
                    sql = "SELECT user_groups_info.group_name FROM user_profile_info , user_groups_info WHERE user_groups_info.person_id = (SELECT user_profile_info.id_person FROM user_profile_info WHERE user_profile_info.name_person = '%s')" %(str(myName))
                    try :
                        #self.transport.write('intra aici')
                        cursor.execute(sql)
                        #self.transport.write('intra aici')

                        results = cursor.fetchall()
                        for result in results :
                            #self.transport.write('intra aici')
                            #self.transport.write('you|'+str(result[0])+'| group\n')
                            names.append(str(result[0]))
                    except:
                        self.transport.write('eroare la primul try')
                    if names == [] :
                        self.transport.write('no groups')
                    else :
                        self.transport.write('your groups are\n')
                    
                        a = set(names)
                        stringGroups = ':'.join(str(e) for e in a)
                        self.transport.write(str(stringGroups)+'\n')
                elif command == "users" :
                    indexUser = 0
                    for beautifuluser in self.beautifulnames :
                        if beautifuluser == content :
                            
                            str1 = ':'.join(str(e) for e in self.beautifulnames)
                            str1 = "".join(str1.split("\r\n"))
                            self.transport.write("Users:"+str1)
                elif command == "getgroups":
                    self.db = pymysql.connect("localhost","testuser2","test123","testasd")
                    userName = a[1]
                    extracontent = a[2]
                    cursor = self.db.cursor()
                    
                    sql = "SELECT DISTINCT user_groups_info.group_name FROM user_profile_info , user_groups_info WHERE user_groups_info.person_id <> (SELECT user_profile_info.id_person FROM user_profile_info WHERE user_profile_info.name_person = '%s')" %(str(userName))

                    try :
                        cursor.execute(sql)
                        results = cursor.fetchall()
                            #for result in results :
                            #self.transport.write(str(results[0][0]) + ' saddsa')
                        possiblegroups = []
                        for result in results :
                            possiblegroups.append(str(result[0]))
                    #        self.transport.write(id + "is the id")
                            #id = results[0]
                            #self.transport.write(id +"is the id")
                            stringsmth = ':'.join(str(e) for e in possiblegroups)
                        self.transport.write('available groups\ngroups:' + stringsmth )
                    except :
                        self.transport.write("No such user")
                elif command == "myuserscoordinates" :
                    self.db = pymysql.connect("localhost","testuser2","test123","testasd")
                    
                    userName = a[1]
                    content = a[2]
                    cursor = self.db.cursor()
                    myfriends = []
                    userLocations = [()]
                    sql = """ (SELECT second_person_id FROM user_friends_info WHERE first_person_id = (SELECT id_person FROM user_profile_info WHERE name_person = BINARY '%s')) """ %(str(userName))
                    try :
                        cursor.execute(sql)
                        results = cursor.fetchall()
                        for result in results :
                            #self.transport.write( str(result[0]) + ' sad')
                            idOfFriend = int(result[0])
                            sqltwo = "SELECT name_person FROM user_profile_info WHERE id_person ='%d'" %(idOfFriend)
                            try:
                                
                                cursor.execute(sqltwo)
                                results = cursor.fetchall()
                                for resulti in results :
                                    nameofFriend = resulti[0]
                                    #self.transport.write(str(nameofFriend))
                                    myfriends.append(nameofFriend)
                            except:
                                self.transport.write('nothing is fishy')
                    except:
                        self.transport.write('something is fishy')
                    stringFriends = ':'.join(str(e) for e in myfriends)
                    #self.transport.write(str(stringFriends))
                    onlinefriends=[]
                    for friend in myfriends :
                        newFriend = str(friend) +'\r\n'
                            #  self.transport.write(newFriend)
                        for e in self.beautifulnames :
                            if e == newFriend :
                                #self.transport.write('ceva cu mere'+ str(a)) ajunge aici

                                for tuple in self.locations :
                                    nameofSomeone = tuple[0]
                                    #self.transport.write('one name'+nameofSomeone) imi face bine aici
                                    ceva = str(nameofSomeone)+'\r\n'
                                    if ceva == str(newFriend) :
                                        a = (nameofSomeone , tuple[1] , tuple[2])
                                        #self.transport.write('ceva'+ str(a))
                                        userLocations.append(a)
                                onlinefriends.append(e)
                    
                    
                    if stringFriends == "" :
                        self.transport.write('no friends online')
                    else:
                        str1 = ''.join(':'.join(str(y) for y in x) for x in set(userLocations))
                        self.transport.write('Coordinates \n'+ str1 )
                        stringFriends = ':'.join(str(e) for e in onlinefriends)
                    #self.transport.write('onlineFriends:'+stringFriends)
                            
               #form myusers:'nameofperson':asdsad
               #I put the last thing there because by default python inserts a \r\n after the last word
                elif command == "myusers" :
                    self.db = pymysql.connect("localhost","testuser2","test123","testasd")

                    userName = a[1]
                    content = a[2]
                    cursor = self.db.cursor()
                    myfriends = []
                    sql = """ (SELECT second_person_id FROM user_friends_info WHERE first_person_id = (SELECT id_person FROM user_profile_info WHERE name_person = BINARY '%s')) """ %(str(userName))
                    try :
                        cursor.execute(sql)
                        results = cursor.fetchall()
                        for result in results :
                    #self.transport.write( str(result[0]) + ' sad')
                            idOfFriend = int(result[0])
                            sqltwo = "SELECT name_person FROM user_profile_info WHERE id_person ='%d'" %(idOfFriend)
                            try:
                                cursor.execute(sqltwo)
                                results = cursor.fetchall()
                                for resulti in results :
                                    nameofFriend = resulti[0]
                                    myfriends.append(nameofFriend)
                            except:
                                self.transport.write('nothing is fishy')
                    except:
                        self.transport.write('something is fishy')
                    stringFriends = ':'.join(str(e) for e in myfriends)
                    self.transport.write('your friends are: \n')
                    self.transport.write(stringFriends +' ')
                elif command == "onlinefriends" :
                    self.db = pymysql.connect("localhost","testuser2","test123","testasd")
                    
                    userName = a[1]
                    content = a[2]
                    cursor = self.db.cursor()
                    myfriends = []
                    sql = """ (SELECT second_person_id FROM user_friends_info WHERE first_person_id = (SELECT id_person FROM user_profile_info WHERE name_person = BINARY '%s')) """ %(str(userName))
                    try :
                        cursor.execute(sql)
                        results = cursor.fetchall()
                        for result in results :
                            #self.transport.write( str(result[0]) + ' sad')
                            idOfFriend = int(result[0])
                            sqltwo = "SELECT name_person FROM user_profile_info WHERE id_person ='%d'" %(idOfFriend)
                            try:
                                cursor.execute(sqltwo)
                                results = cursor.fetchall()
                                for resulti in results :
                                    nameofFriend = resulti[0]
                                    myfriends.append(nameofFriend)
                            except:
                                self.transport.write('nothing is fishy')
                    except:
                        self.transport.write('something is fishy')
                        stringFriends = ':'.join(str(e[1:]) for e in myfriends)
                    for e in myfriends :
                        x = 1
                        #self.transport.write('wowww'+ e[1:] + ' is my friend\n')
                    onlinefriends=[]
                    for friend in myfriends :
                        name = friend
                        newFriend = str(name) +'\r\n'
                        for e in self.beautifulnames :
                            if e == newFriend :
                                onlinefriends.append(name)
                    stringFriends = ':'.join(str(e) for e in set(onlinefriends))
                    if stringFriends == "" :
                        self.transport.write('no friends online')
                    else:
                        self.transport.write('onlineFriends:\n'+stringFriends)
                elif command == "possiblefriends" :
                    self.db = pymysql.connect("localhost","testuser2","test123","testasd")
                    cursor = self.db.cursor()
                    myName = a[1]
                    ceva = a[2]
                    possiblefriends = []
                    sql = "SELECT name_person FROM user_profile_info WHERE name_person <> '%s'" %(myName)
                    try :
                        cursor.execute(sql)
                        results = cursor.fetchall()
                        for result in results :
                            namele = result[0]
                            possiblefriends.append(namele)
                        allfriends = ':'.join(str(e) for e in possiblefriends)
                        self.transport.write('PossibleFriends:\n'+allfriends+'\n')
                    except :
                        self.transport.write('Fishy fishy fishy')
                elif command == "searchphone" :
                    phoneNumber = a[1]
                    ceva = a[2]
                    self.db = pymysql.connect("localhost","testuser2","test123","testasd")
                    cursor = self.db.cursor()
                    sql = "SELECT name_person FROM user_profile_info WHERE phone_number = '%s'" %(phoneNumber)
                    #self.transport.write('ndsadas')
                    try :
                        cursor.execute(sql)
                        results = cursor.fetchall()
                        self.transport.write('person:'+str(results[0][0]) +'\n')
                        #self.transport('person:'+str(results[0]))
                    except:
                        self.transport.write('no such number'+'\n')
                elif command == "searchname" :
                    nameToSearch = a[1]
                    ceva = a[2]
                    self.db = pymysql.connect("localhost","testuser2","test123","testasd")
                    cursor = self.db.cursor()
                    sql = "SELECT name_person from user_profile_info WHERE name_person = '%s'" %(nameToSearch)
                    try :
                        cursor.execute(sql)
                        results = cursor.fetchall()
                        self.transport.write('person:'+str(results[0][0])+'\n')
                    except :
                        self.transport.write('no such person\n')
                elif command == "saveconversation":
                    person1 = a[1]
                    person2 = a[2]
                    conversation = a[3]
                    content = a[4]
                    location = "people-conversation/"+str(person1)+"-"+str(person2)+".txt"
                    try :
                        self.transport.write("conversation saved")
                        f = open(location,'wa+')
                        f.write(conversation)
                
                    except:
                        self.transport.write('error opening the file')
                elif command == "getconversation":
                    person1 = a[1]
                    person2 = a[2]
                    content = a[3]
                    location = "people-conversation/"+str(person1)+"-"+str(person2)+".txt"
                    try :
                        self.transport.write('Conversation:')
                        #f = open(location,'w+')
                        for bytes in read_bytes_from_file(location) :
                            
                            self.transport.write(bytes)
                            self.transport.write('')
                    except :
                        self.transport.write('no such file\r\n')
                elif command == "saveimage":
                    personName = a[1]
                    contentData = a[2]
                    escape = a[3]
                    location = "person-images/"+str(personName)+".jpg"
                    try:
                        f = open (location,'w+')
                        f.write(contentData)
                    except :
                        self.transport.write("error when opening the file")
                elif command == "getimage" :
                    picture = a[1]
                    extra = a[2]
                    location = "person-images/"+picture+".png"
                    try:
                        self.transport.write("image:")
                        for bytes in read_bytes_from_file(location) :
                            self.transport.write(bytes)
                        self.transport.write('\r\n')
                    except :
                        self.transport.write('no such file\r\n')
                elif command == "modifyall" :
                    self.db = pymysql.connect("localhost","testuser2","test123","testasd")
                    cursor = self.db.cursor()

                    name = a[1]
                    password = a[2]
                    email = a[3]
                    phoneNumber = a[4]
                    extra = a[5]
                    sql = """UPDATE `user_profile_info` SET `person_email`='%s',`password`='%s',`phone_number`='%s' WHERE name_person = '%s'""" %(str(email),str(password),str(phoneNumber),str(name))
                    try :
                        cursor.execute(sql)
                        self.db.commit()
                        self.transport.write('successfully modified')
                    except:
                        self.transport.write("couldn't modify you")
                elif command == "modifyemail" :
                    self.db = pymysql.connect("localhost","testuser2","test123","testasd")
                    cursor = self.db.cursor()
                    name = a[1]
                    email = a[2]
                    extra = a[3]
                    sql = """UPDATE `user_profile_info` SET `person_email`='%s' WHERE name_person = '%s'""" %(str(email),str(name))
                    try :
                        cursor.execute(sql)
                        self.db.commit()
                    except :
                        self.transport.write("couldn't modify you")
                elif command == "modifypass" :
                    self.db = pymysql.connect("localhost","testuser2","test123","testasd")
                    cursor = self.db.cursor()
                    name = a[1]
                    password = a[2]
                    extra = a[3]
                    sql = """UPDATE `user_profile_info` SET `password`='%s' WHERE name_person = '%s'""" %(str(password),str(name))
                    try :
                        cursor.execute(sql)
                        self.db.commit()
                    except :
                        self.transport.write("couldn't modify you")
                elif command == "modifyphone":
                  
                    self.db = pymysql.connect("localhost","testuser2","test123","testasd")
                    cursor = self.db.cursor()
                    name = a[1]
                    phoneNumber = a[2]
                    extra = a[3]
                    sql = """UPDATE `user_profile_info` SET `phone_number`='%s'WHERE name_person = '%s'""" %(str(phoneNumber),str(name))
                    try :
                        cursor.execute(sql)
                        self.db.commit()
                    except :
                        self.transport.write("couldn't modify you")
                elif command == "groupsusers":
                    personName = a[1]
                    nameGroup = a[2]
                    content = a[3]
                    self.db = pymysql.connect("localhost","testuser2","test123","testasd")
                    groupsusers = []
                    cursor = self.db.cursor()
                    onlineUsers = []
                    sql = """SELECT person_id FROM user_groups_info WHERE group_name = '%s'""" %(str(nameGroup))
                    try:
                        cursor.execute(sql)
                        results = cursor.fetchall()
                        for result in results :
                            # groupsusers.append(int(result[0]))
                            #stringusers = ':'.join(str(e) for e in set(groupsusers))
#self.transport.write("users are " + stringusers)
                            id = int(result[0])
                            sql = """SELECT name_person FROM user_profile_info WHERE id_person = '%d' AND name_person <> '%s'""" %(id,str(personName))
                            try:
                                cursor.execute(sql)
                                results = cursor.fetchall()
                                #self.transport.write('results are ' + str(results))
                                for result in results :
                                    groupsusers.append(str(result[0]))
                            except :
                                self.transport.write('problem with second query')
                        

                        for possibleuser in groupsusers :
                            serverName = possibleuser+'\r\n'
                            for person in self.beautifulnames :
                                if person == serverName :
                                    onlineUsers.append(str(possibleuser))
#self.transport.write('we have this one')
                        if onlineUsers == [] :
                            self.transport.write('no online users')
                        else :
                            strfriends = ':'.join(str(e) for e in onlineUsers)
                            self.transport.write('users:' + str(strfriends)+'\n')
                    except:
                        self.transport.write("problem with query")
                elif command == "isonline":
                    hisname = a[1]+'\r\n'
                    content = a[2]
                    found = False
                    for user in self.beautifulnames :
                        if user == hisname :
                            found = True
                    if found == True :
                        self.transport.write("user online")
                    else :
                        self.transport.write("user not online")


#UPDATE `user_profile_info` SET `id_person`= '213213',`name_person`='tudoriCAacasa',`person_email`='sasdsa',`password`='qweqweqw',`phone_number`='2123123' WHERE name_person = 'tudoricaacasa'
#def read_bytes_from_file(file, chunk_size = 8100):
#    """ Read bytes from a file in chunks. """
#        
#        with open(file, 'rb') as file:
#            while True:
#                chunk = file.read(chunk_size)
#                
#                if chunk:
#                    yield chunk
#                else:
#                    break
factory = Factory()
factory.protocol=IphoneChat
factory.clients = []
reactor.listenTCP(8023,factory)
print "IPhone Chat server started"
reactor.run()