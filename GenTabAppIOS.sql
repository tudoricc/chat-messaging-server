
DROP TABLE IF EXISTS user_friends_info;
DROP TABLE IF EXISTS user_groups_info;
DROP TABLE IF EXISTS user_profile_info;
DROP TABLE IF EXISTS groups_users;
DROP TABLE IF EXISTS groups_users_info;
DROP TABLE IF EXISTS user_friends;


CREATE TABLE user_profile_info(
	id_person INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
	name_person VARCHAR(21) NOT NULL, person_email VARCHAR(25) NOT NULL ,
	password VARCHAR(12) NOT NULL , phone_number VARCHAR(25) NOT NULL ,
	UNIQUE KEY(name_person ,person_email , phone_number )
	)ENGINE=InnoDB DEFAULT CHARSET = latin1 ;


CREATE TABLE user_friends_info(
	id_relationship INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
	first_person_id INT(11) UNSIGNED NOT NULL,
	second_person_id INT(11) UNSIGNED NOT NULL,
	CONSTRAINT fk1_person_id FOREIGN KEY(first_person_id) REFERENCES user_profile_info(id_person),
	CONSTRAINT fk2_person_id FOREIGN KEY(second_person_id) REFERENCES user_profile_info(id_person))ENGINE=InnoDB DEFAULT CHARSET = latin1 ;

CREATE TABLE user_groups_info(
	id_group INT(11) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY , 
	group_name VARCHAR(21) NOT NULL ,
	person_id INT(11) UNSIGNED NOT NULL,
	CONSTRAINT fk3_person_of_group_id FOREIGN KEY(person_id) REFERENCES user_profile_info(id_person))ENGINE=InnoDB DEFAULT CHARSET =  latin1 ;  							









						  